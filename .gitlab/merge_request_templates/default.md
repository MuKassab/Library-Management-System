## Development/Release Checks

- Related to: _Insert Jira Ticket Here_
- [ ] Queries reviewed
- Missing indexes list to be created
  - [ ] Yes
  ```
    If Yes insert indices here
  ```
  - [ ] No
- Scripts needed to run before/after deployment ?
  - [ ] Yes
  ```
    If Yes insert indices here
  ```
  - [ ] No

---

## Release checks (Before merging to master)

- [ ] Performance check on Beta

---

## After release checks (Please keep an eye on the following)

- Check the Database CPU
- Check the error rate
- Check the functionality is working as expected on production
